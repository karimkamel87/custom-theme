<?php
get_header();



if (have_posts()) {
    while (have_posts()) {
        the_post();
        page_banner()


?>


        <div class="container container--narrow page-section">
            <?php
            $postId = get_the_ID();
            $parentId = wp_get_post_parent_id($postId);
            $isParent = $parentId === 0;
            if (!$isParent) {

            ?>

                <div class="metabox metabox--position-up metabox--with-home-link">
                    <p><a class="metabox__blog-home-link" href="<?php echo get_the_permalink($parentId) ?>"><i class="fa fa-home" aria-hidden="true"></i> <?php echo get_the_title($parentId) ?></a> <span class="metabox__main"><?php the_title(null, null) ?></span></p>
                </div>

            <?php
            }




            if ($isParent) {
                //if current page is parent set "title" to current page title and "parentPageId" to current page id
                $title = get_the_title();
                $parentPageId = $postId;
            } else {
                //if current page is NOT parent set "title" to parent page title and "parentPageId" to parent page id
                $title = get_the_title($parentId);
                $parentPageId = $parentId;
            }
            $parentPageLink = get_the_permalink($parentPageId);
            $hasChildren = count(get_children($postId)) > 0;


            //avoid displaying side list of subpages if this is a parent page with no children
            if (!$isParent || $hasChildren) { ?>
                <div class="page-links">
                    <h2 class="page-links__title"><a href="<?php echo $parentPageLink ?>"><?php echo $title ?></a></h2>
                    <ul class="min-list">
                        <?php wp_list_pages(array('child_of' => $parentPageId, 'title_li' => null)) ?>

                    </ul>
                </div>
            <?php } ?>




            <div class="generic-content">
                <?php get_search_form() ?>
            </div>

        </div>
<?php

    }
}
get_footer();
?>