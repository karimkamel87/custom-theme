<?php
get_header();

function mapFunction($arg)
{
    return $arg->post_title;
}

if (have_posts()) {
    while (have_posts()) {
        the_post();
        page_banner()




?>


        <div class="container container--narrow page-section">
            <?php
            $postId = get_the_ID();
            $parentId = wp_get_post_parent_id($postId);
            $parsed =  wp_parse_url(wp_get_referer(),);
            $parsedPath = $parsed['path'];
            $theType = gettype($parsedPath);

            $pieces = explode("/", $parsedPath);



            ?>


            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link("event")
                                                            ?>"><i class="fa fa-home" aria-hidden="true"></i> back to all events</a> <span class="metabox__main">
                        <?php the_title() ?>
                    </span></p>
            </div>











            <div class="generic-content">
                <?php the_content() ?>
            </div>
            <div>
                <?php
                $relatedPrograms = get_field("related_programs");

                if ($relatedPrograms) {
                ?>
                    <hr class="section-break">
                    <h2 class="headline headline--medium">
                        this event is related to the following program(s)</h2>
                    <ul class="link-list min-list">
                        <?php

                        array_map(function ($program) {
                        ?>
                            <li><a href="<?php echo get_the_permalink($program) ?>"><?php echo get_the_title($program) ?></a></li>
                            <!-- echo "<li>" . get_the_title($program) . "</li>";
                            // echo "<li>" . $program->post_title . "</li>"; -->
                        <?php
                        }, $relatedPrograms)


                        ?>
                    </ul>
                <?php } ?>
                <!-- <?php
                        if ($relatedPrograms) {
                        ?>
                    <p>
                        this event is related to the following program(s)</p>
                    <ul>
                        <?php

                            foreach ($relatedPrograms as &$program) {
                                echo
                                "<li>$program->post_title </li>";
                            }


                        ?>
                    </ul>
                <?php } ?> -->
                <!-- <?php
                        if ($programTitles) {
                        ?>
                    <p>
                        this event is related to the following program(s)</p>
                    <ul>
                        <?php

                            foreach ($programTitles as &$title) {
                                echo
                                "<li>$title</li>";
                            }


                        ?>
                    </ul>
                <?php } ?> -->
            </div>

        </div>
<?php

    }
}
get_footer();
?>