<?php
get_header();
page_banner(array(
    "title" => "All Programs",
    "subtitle" => "Available Programs"
))
?>



<div class="container container--narrow page-section">
    <ul class="link-list min-list">
        <?php

        while (have_posts()) {
            the_post();



        ?>
            <li><a href="<?php the_permalink() ?>"><?php the_title() ?> </a></li>



        <?php
            // paginate_links()
            // the_title();
            // the_content( );
        }


        the_posts_pagination();

        ?>
    </ul>


</div>


<?php
get_footer();
?>