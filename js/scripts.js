// import "../css/style.css"
import $ from "jquery";
import slick from "slick-carousel";

// Our modules / classes
import GoogleMap from "./modules/GoogleMap";
import MobileMenu from "./modules/MobileMenu";
import HeroSlider from "./modules/HeroSlider";
import Search from "./modules/Search";
import MyNotes from "./modules/MyNotes";
import Like from "./modules/Like";

// Instantiate a new object using our modules/classes
var mobileMenu = new MobileMenu();
var heroSlider = new HeroSlider();
var googleMap = new GoogleMap();
var search = new Search();
var myNotes = new MyNotes();
var like = new Like();
// Allow new JS and CSS to load in browser without a traditional page refresh
if (module.hot) {
  module.hot.accept();
}
