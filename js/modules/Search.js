import $, { post } from "jquery";
import regeneratorRuntime from "regenerator-runtime";
class Search {
  // initiate object
  constructor() {
    this.addSearchHtml();
    this.openButton = $(".js-search-trigger");
    this.closeButton = $(".search-overlay__close");
    this.searchOverlay = $(".search-overlay");
    this.searchField = $("#search-term");
    this.events();
    this.isOverlayOpen = false;
    this.isTimerStarted = false;
    this.typingTimer;
    this.resultsDiv = $("#search-overlay__results");
    this.isSpinnerVisible = false;
    this.previousValue;
  }

  //events
  events() {
    this.searchField.on("keyup", this.typingLogic.bind(this));
    this.openButton.on("click", this.openOverlay.bind(this));
    this.closeButton.on("click", this.closeOverlay.bind(this));
    $(document).on("keyup", this.keyPressDispatcher.bind(this));
  }
  keyPressDispatcher(e) {
    if (
      e.keyCode === 83 &&
      !this.isOverlayOpen &&
      !$("input, textarea").is(":focus")
    )
      this.openOverlay();
    if (e.keyCode === 27 && this.isOverlayOpen) this.closeOverlay();
  }
  typingLogic(e) {
    if (this.searchField.val() !== this.previousValue) {
      clearTimeout(this.typingTimer);
      if (this.searchField.val()) {
        if (!this.isSpinnerVisible) {
          var spinner = $("<div>").addClass("spinner-loader");
          this.resultsDiv.html(spinner);
          this.isSpinnerVisible = true;
        }
        this.typingTimer = setTimeout(this.getResults.bind(this), 750);
        this.previousValue = this.searchField.val();
      } else {
        this.resultsDiv.html("");
        this.isSpinnerVisible = false;
      }
    }
  }
  //methods

  // getResults() {
  //   //use  wordpress search endpoint to retrieve both pages and posts
  //   $.getJSON(
  //     universityData.root_url +
  //       `/wp-json/wp/v2/search?search=${this.searchField.val()}`,
  //     (posts) => {
  //       this.resultsDiv.html(`
  //       <h2 class = "search-overlay__section-title">General information</h2>

  //       ${
  //         // opening ul or "no results message"
  //         posts.length
  //           ? `<ul class = "link-list min-list">`
  //           : `<p>No general information found</p>`
  //       }

  //       ${posts
  //         //output posts (if any)
  //         .map((post) => `<li><a href ="${post.link}">${post.title}</a></li>`)
  //         .join("")}

  //       ${posts.length ? `</ul>` : ""}`);
  //       this.isSpinnerVisible = false;
  //     }
  //   );
  // }

  // two queries; one for pages one for posts. Called in an asynchronous fashion using
  //  async await
  // async getResults() {
  //   const posts = await $.getJSON(
  //     universityData.root_url +
  //       `/wp-json/wp/v2/posts?search=${this.searchField.val()}`
  //   );
  //   const pages = await $.getJSON(
  //     universityData.root_url +
  //       `/wp-json/wp/v2/pages?search=${this.searchField.val()}`
  //   );

  //   var combinedResults = posts.concat(pages);
  //   this.resultsDiv.html(`
  //       <h2 class = "search-overlay__section-title">General information</h2>

  //       ${
  //         // opening ul or "no results message"
  //         combinedResults.length
  //           ? `<ul class = "link-list min-list">`
  //           : `<p>No general information found</p>`
  //       }

  //       ${combinedResults
  //         //output combinedResults (if any)
  //         .map(
  //           (post) =>
  //             `<li><a href ="${post.link}">${post.title.rendered}</a></li>`
  //         )
  //         .join("")}

  //       ${combinedResults.length ? `</ul>` : ""}`);
  //   this.isSpinnerVisible = false;
  // }

  // two queries; one for pages one for posts. Called in an asynchronous fashion using Jquery when.then
  getResults() {
    $.getJSON(
      universityData.root_url +
        `/wp-json/university/v1/search?term=${this.searchField.val()}`,
      (results) => {
        this.resultsDiv.html(`
      <div class="row">
        <div class="one-third">
          <h2 class = "search-overlay__section-title">General Information</h2>
          ${
            // opening ul or "no results message"
            results.generalInfo.length
              ? `<ul class = "link-list min-list">`
              : `<p>No general information found</p>`
          }

        ${results.generalInfo
          //output combinedResults (if any)
          .map(
            (post) =>
              `<li><a href ="${post.permalink}">${post.title}</a>${
                post.postType === "post" ? ` posted by ${post.authorName}` : ""
              } </li>`
          )
          .join("")}

        ${results.generalInfo.length ? `</ul>` : ""}
        </div>
        <div class="one-third">
          <h2 class = "search-overlay__section-title">Programs</h2>
          ${
            // opening ul or "no results message"
            results.programs.length
              ? `<ul class = "link-list min-list">`
              : `<p>No programs information found<a href="${universityData.root_url}/programs"> view all programs</a></p>`
          }

        ${results.programs
          //output combinedResults (if any)
          .map(
            (post) => `<li><a href ="${post.permalink}">${post.title}</a></li>`
          )
          .join("")}

        ${results.programs.length ? `</ul>` : ""}
          <h2 class = "search-overlay__section-title">Professors</h2>
          ${
            // opening ul or "no results message"
            results.professors.length
              ? `<ul class = "professor-cards">`
              : `<p>No professor information found</p>`
          }

        ${results.professors
          //output combinedResults (if any)
          .map(
            (post) => `
            <li class="professor-card__list-item">
              <a class="professor-card" href="${post.permalink}">
              <img class="professor-card__image" src=${post.image}>>
              <span class="professor-card__name">${post.title}</span>
              </a>
            </li>                            
            `
          )
          .join("")}

        ${results.professors.length ? `</ul>` : ""}
          
        </div>
        <div class="one-third">
        <h2 class = "search-overlay__section-title">Campuses</h2>
        ${
          // opening ul or "no results message"
          results.campuses.length
            ? `<ul class = "link-list min-list">`
            : `<p>No campus information found <a href="${universityData.root_url}/campuses"> view all campuses</a></p>`
        }

        ${results.campuses
          //output combinedResults (if any)
          .map(
            (post) => `<li><a href ="${post.permalink}">${post.title}</a></li>`
          )
          .join("")}

        ${results.campuses.length ? `</ul>` : ""}
        <h2 class = "search-overlay__section-title">Events</h2>
        ${
          // opening ul or "no results message"
          results.events.length
            ? ``
            : `<p>No events found <a href= ${universityData.root_url}/events> view all events</a> </p>`
        }  

        ${results.events
          //output combinedResults (if any)
          .map(
            (post) => `
             
 <div class="event-summary">
     <a class="event-summary__date t-center" href="${post.permalink}">
         <span class="event-summary__month">${post.month}</span>
         <span class="event-summary__day">${post.day}</span>
     </a>
     <div class="event-summary__content">
         <h5 class="event-summary__title headline headline--tiny"><a href="${post.permalink}">${post.title}</a></h5>
         <p>${post.description} <a href="${post.permalink}" class="nu gray">Learn more</a></p>
     </div>
 </div>                       
            `
          )
          .join("")}

        ${results.events.length ? `</ul>` : ""}
        </div>
      </div>`);
        this.isSpinnerVisible = false;
      }
    );
  }

  //two queries; one for pages one for posts. Called in a synchronous fashion
  // getResults() {
  //   $.getJSON(
  //     universityData.root_url +
  //       `/wp-json/wp/v2/posts?search=${this.searchField.val()}`,
  //     (posts) => {
  //       $.getJSON(
  //         universityData.root_url +
  //           `/wp-json/wp/v2/pages?search=${this.searchField.val()}`,
  //         (pages) => {
  //           var combinedResults = posts.concat(pages);
  //           this.resultsDiv.html(`
  //       <h2 class = "search-overlay__section-title">General information</h2>

  //       ${
  //         // opening ul or "no results message"
  //         combinedResults.length
  //           ? `<ul class = "link-list min-list">`
  //           : `<p>No general information found</p>`
  //       }

  //       ${combinedResults
  //         //output combinedResults (if any)
  //         .map(
  //           (post) =>
  //             `<li><a href ="${post.link}">${post.title.rendered}</a></li>`
  //         )
  //         .join("")}

  //       ${combinedResults.length ? `</ul>` : ""}`);
  //           this.isSpinnerVisible = false;
  //         }
  //       );
  //     }
  //   );
  // }

  openOverlay() {
    this.searchOverlay.addClass("search-overlay--active");
    $("body").addClass("body-no-scroll");
    setTimeout(() => this.searchField.focus(), 301);
    this.isOverlayOpen = true;
    return false;
  }
  closeOverlay(event) {
    this.searchOverlay.removeClass("search-overlay--active");
    $("body").removeClass("body-no-scroll");
    this.isOverlayOpen = false;
    this.searchField.val("");
  }
  addSearchHtml() {
    $("body").append(`
    <div class="search-overlay">
  <div class="search-overlay__top">
    <div class="container">
      <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
      <input type="text" class="search-term" placeholder="What are you looking for?" id="search-term">
      <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>

    </div>
  </div>
  <div class="container">
    <div id="search-overlay__results"></div>
  </div>
</div>`);
  }
}
export default Search;
