import $ from "jquery";
class Like {
  constructor() {
    this.likeBox = $(".like-box");
    this.likeExists = $(".like-box").data("exists");
    this.events();
    console.log(this.likeExists);
  }
  events() {
    $(".like-box").on("click", this.handleClick.bind(this));
  }
  handleClick(e) {
    const currentLikeBox = $(e.target.closest(".like-box"));
    if (currentLikeBox.attr("data-exists") === "yes")
      this.unlike(currentLikeBox);
    else this.like(currentLikeBox);
  }
  like(currentLikeBox) {
    $.ajax({
      beforeSend: (xhr) => {
        xhr.setRequestHeader("X-WP-NONCE", universityData.nonce);
      },
      url: universityData.root_url + "/wp-json/university/v1/manageLike",
      type: "POST",
      data: { professorId: currentLikeBox.data("professor") },
      success: (response) => {
        console.log(response);
        currentLikeBox.attr("data-exists", "yes");

        var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount++;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", response);
      },
      error: (response) => console.log(response),
    });
  }
  unlike(currentLikeBox) {
    $.ajax({
      beforeSend: (xhr) => {
        xhr.setRequestHeader("X-WP-NONCE", universityData.nonce);
      },
      url: universityData.root_url + "/wp-json/university/v1/manageLike",
      type: "DELETE",
      data: { like: currentLikeBox.attr("data-like") },
      success: (response) => {
        console.log(response);
        currentLikeBox.attr("data-exists", "no");

        var likeCount = parseInt(currentLikeBox.find(".like-count").html(), 10);
        likeCount--;
        currentLikeBox.find(".like-count").html(likeCount);
        currentLikeBox.attr("data-like", "");
      },
      error: (response) => console.log(response),
    });
    currentLikeBox.attr("data-exists", "no");
    currentLikeBox.data("exists", "no");
  }
}
export default Like;
