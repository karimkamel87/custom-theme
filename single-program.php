<?php
get_header();



if (have_posts()) {
    //loop through programs
    while (have_posts()) {
        the_post();
        page_banner()
?>


        <div class="container container--narrow page-section">



            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link("program")
                                                            ?>"><i class="fa fa-home" aria-hidden="true"></i> back to programs</a> <span class="metabox__main">
                        <?php the_title() ?>
                    </span></p>
            </div>

            <div class="generic-content">
                <?php the_field("main_body_content") ?>
            </div>

            <?php
            $args = array(
                "posts_per_page" => -1,
                "post_type" => "professor",
                "orderby" => "title",
                "order" => "asc",
                "meta_query" => array(
                    array(
                        "key" => "related_programs",
                        //value needs to be surrounded by double quotes because how custom fields records data in the database.
                        "value" => '"' . get_the_id() . '"',
                        "compare" => "like"
                    )
                )
            );
            //for current program get professors with extra custom field equal to current program ID
            $relatedProfessors  = new WP_Query($args);
            if ($relatedProfessors->have_posts()) {
            ?>
                <hr class="section-break">

                <h2 class="headline headline--medium"> Subject is taught by: </h2>
                <ul class="professor-cards">
                    <?php
                    while ($relatedProfessors->have_posts()) {
                        $relatedProfessors->the_post(); ?>
                        <li class="professor-card__list-item"><a class="professor-card" href="<?php the_permalink() ?>">


                                <img class="professor-card__image" src=<?php the_post_thumbnail_url("professorLandscape") ?>>


                                <span class="professor-card__name"><?php the_title() ?></span>

                            </a></li>
                    <?php
                    }
                    ?>
                </ul>
            <?php
            }
            //dont forget to reset postdata
            wp_reset_postdata();

            $the_title = get_the_title();
            $the_ID = get_the_ID();
            $today = date("Ymd");

            $args = array(
                'posts_per_page' => 2,
                'post_type' =>  'event',
                // "orderby" => "meta_value_datetime",
                "orderby" => "meta_value_num",

                "meta_key" => "event_date",
                "order" => "asc",
                "meta_query" => array(
                    array(
                        "key" => "event_date",
                        "value" => $today,
                        "compare" => ">=",
                        "type" => "numeric"

                    ),
                    array(
                        "key" => "related_programs",
                        "value" => $the_ID,
                        "compare" => "LIKE"

                        // "type" => "numeric"

                    )
                )
            );
            // for current program get events with date greater or equal to today
            // and with program ID in related programs
            $programEvents = new WP_Query($args);
            if ($programEvents->have_posts()) {
            ?>
                <hr class="section-break">
                <h2 class="headline headline--medium">Upcoming <?php the_title() ?> Events</h2>
                <?php


                while ($programEvents->have_posts()) {
                    $programEvents->the_post();
                    get_template_part("template-parts/content", "event");
                }
            }

            wp_reset_postdata();
            $relatedCampuses = get_field("related_campus");
            if ($relatedCampuses) {
                echo "<hr class = 'section-break'>";
                echo "<h2 class='headline headline--medium'>" . get_the_title() . " is available at these campuses </h2>";
                echo "<ul class='min-list link-list'>";
                foreach ($relatedCampuses as $campus) {
                ?>
                    <li><a href="<?php get_the_permalink($campus) ?>">
                            <?php echo get_the_title($campus); ?>

                        </a></li>
            <?php

                }
                echo "</ul>";
            }
            ?>

        </div>
<?php
    }
}

get_footer();
?>