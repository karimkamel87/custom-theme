<?php
get_header();



if (have_posts()) {
    //loop through programs
    while (have_posts()) {
        the_post();
        page_banner()
?>


        <div class="container container--narrow page-section">



            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link("campus")
                                                            ?>"><i class="fa fa-home" aria-hidden="true"></i> All Campuses</a> <span class="metabox__main">
                        <?php the_title() ?>
                    </span></p>
            </div>

            <div class="generic-content">
                <?php the_content() ?>
            </div>
            <div class="acf-map">
                <div class="marker" data-lat="
                    <?php echo get_field("map_location")["lat"] ?>" data-lng="
                     <?php echo get_field("map_location")["lng"] ?>">
                    <h3><?php the_title() ?>
                    </h3>
                    <?php echo get_field("map_location")["address"] ?>

                </div>




            </div>


            <?php
            $args = array(
                "posts_per_page" => -1,
                "post_type" => "program",
                "orderby" => "title",
                "order" => "asc",
                "meta_query" => array(
                    array(
                        "key" => "related_campus",
                        "value" => '"' . get_the_id() . '"',
                        "compare" => "like"
                    )
                )
            );
            //for current campus get programs with extra custom field equal to current campus ID
            $availablePrograms  = new WP_Query($args);
            if ($availablePrograms->have_posts()) {
            ?>
                <hr class="section-break">

                <h2 class="headline headline--medium"> Programs available at this campus </h2>
                <ul class="min-list link-list">
                    <?php
                    while ($availablePrograms->have_posts()) {
                        $availablePrograms->the_post(); ?>
                        <li>
                            <a href="<?php the_permalink() ?>">
                                <?php the_title() ?>

                            </a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            <?php
            }
            //dont forget to reset postdata
            wp_reset_postdata();

            $the_title = get_the_title();
            $the_ID = get_the_ID();
            $today = date("Ymd");

            $args = array(
                'posts_per_page' => 2,
                'post_type' =>  'event',
                // "orderby" => "meta_value_datetime",
                "orderby" => "meta_value_num",

                "meta_key" => "event_date",
                "order" => "asc",
                "meta_query" => array(
                    array(
                        "key" => "event_date",
                        "value" => $today,
                        "compare" => ">=",
                        "type" => "numeric"

                    ),
                    array(
                        "key" => "related_programs",
                        "value" => $the_ID,
                        "compare" => "LIKE"

                        // "type" => "numeric"

                    )
                )
            );
            // for current program get events with date greater or equal to today
            // and with program ID in related programs
            $programEvents = new WP_Query($args);
            if ($programEvents->have_posts()) {
            ?>
                <hr class="section-break">
                <h2 class="headline headline--medium">Upcoming <?php the_title() ?> Events</h2>
            <?php


                while ($programEvents->have_posts()) {
                    $programEvents->the_post();
                    get_template_part("template-parts/content", "event");
                }
            }
            ?>
        </div>
<?php
    }
}

get_footer();
?>