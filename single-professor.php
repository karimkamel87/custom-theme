<?php
get_header();

function mapFunction($arg)
{
    return $arg->post_title;
}

if (have_posts()) {
    while (have_posts()) {
        the_post();
        page_banner()



?>


        <div class="container container--narrow page-section">
            <div class="generic-content">
                <div class="row group">
                    <div class="one-third">
                        <?php the_post_thumbnail("professorPortrait"); ?></div>
                    <div class="two-thirds">
                        <?php
                        //this query gets total number of likes for professor
                        $likeCount = new WP_Query(array(
                            "post_type" => "like",
                            "meta_query" => array(
                                array(
                                    "key" => "liked_professor_id",
                                    "value" => get_the_ID(),
                                    "compare" => "="
                                )


                            )
                        ));
                        $likeExists = "no";
                        if (is_user_logged_in()) {
                            //this query looks for likes made by the current logged in user for the current professor

                            //if user is not logged in get_the_author_meta('ID'), would evaluate to 0, and filtering for an ID of 0 is equivalent to not filtering for an author
                            $statusQuery = new WP_Query(array(
                                "author" => get_current_user_id(),
                                "post_type" => "like",
                                "meta_query" => array(
                                    array(
                                        "key" => "liked_professor_id",
                                        "value" => get_the_ID(),
                                        "compare" => "="
                                    )


                                )
                            ));
                            if ($statusQuery->post_count) {
                                $likeExists = "yes";
                            }
                        }
                        ?>
                        <span class="like-box" data-like="<?php echo $statusQuery->posts[0]->ID; ?>" data-professor="<?php echo get_the_ID() ?>" data-exists="<?php echo $likeExists ?>"><i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <span class="like-count"><?php echo $likeCount->found_posts ?></span> </span>
                        <?php the_content() ?>
                    </div>
                </div>


            </div>
            <div>
                <?php
                $relatedPrograms = get_field("related_programs");

                if ($relatedPrograms) {
                ?>
                    <hr class="section-break">
                    <h2 class="headline headline--medium">
                        Teaches the following subjects(s)</h2>
                    <ul class="link-list min-list">
                        <?php

                        array_map(function ($program) {
                        ?>
                            <li><a href="<?php echo get_the_permalink($program) ?>"><?php echo get_the_title($program) ?></a></li>
                            <!-- echo "<li>" . get_the_title($program) . "</li>";
                            // echo "<li>" . $program->post_title . "</li>"; -->
                        <?php
                        }, $relatedPrograms)


                        ?>
                    </ul>
                <?php } ?>
                <!-- <?php
                        if ($relatedPrograms) {
                        ?>
                    <p>
                        this event is related to the following program(s)</p>
                    <ul>
                        <?php

                            foreach ($relatedPrograms as &$program) {
                                echo
                                "<li>$program->post_title </li>";
                            }


                        ?>
                    </ul>
                <?php } ?> -->

            </div>

        </div>
<?php

    }
}
get_footer();
?>