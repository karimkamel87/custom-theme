<?php
get_header();
page_banner(array(
    "title" => "Our Campuses",
    "subtitle" => "We have several campuses across the city."
))
?>



<div class="container container--narrow page-section">

    <div class="acf-map">
        <?php

        while (have_posts()) {
            the_post();



        ?>
            <div class="marker" data-lat="
            <?php echo get_field("map_location")["lat"] ?>" data-lng="
             <?php echo get_field("map_location")["lng"] ?>">
                <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                </h3>
                <?php echo get_field("map_location")["address"] ?>

            </div>



        <?php

        }




        ?>
    </div>


</div>


<?php
get_footer();
?>