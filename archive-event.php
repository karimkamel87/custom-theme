<?php
get_header();
page_banner(array(
    "title" => "All Events",
    "subtitle" => "What is going on in the university"
))
?>



<div class="container container--narrow page-section">
    <?php

    while (have_posts()) {
        the_post();
        get_template_part("template-parts/content", "event");
    }
    the_posts_pagination();

    ?>
    <hr class="section-link">
    <p>Check out our past events <a href="<?php echo site_url("/past-events") ?>">archive.</a> </p>
</div>


<?php
get_footer();
?>