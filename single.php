<?php
get_header();



if (have_posts()) {
    while (have_posts()) {
        the_post();
        page_banner(array(
            "title" => get_the_title()
        ))

?>


        <div class="container container--narrow page-section">
            <?php
            $postId = get_the_ID();
            $parentId = wp_get_post_parent_id($postId);
            $parsed =  wp_parse_url(wp_get_referer(),);
            $parsedPath = $parsed['path'];
            $theType = gettype($parsedPath);

            $pieces = explode("/", $parsedPath);



            ?>


            <div class="metabox metabox--position-up metabox--with-home-link">
                <p><a class="metabox__blog-home-link" href="<?php echo site_url("/blog")
                                                            ?>"><i class="fa fa-home" aria-hidden="true"></i> back to blog</a> <span class="metabox__main">
                        Posted by <?php the_author_posts_link() ?> on <?php the_time("F j Y") ?> in <?php
                                                                                                    echo get_the_category_list(", ") ?>
                    </span></p>
            </div>











            <div class="generic-content">
                <?php the_content() ?>
            </div>

        </div>
<?php

    }
}
get_footer();
?>