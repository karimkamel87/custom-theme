<?php
function universityLikeRoutes()
{
    //post method
    register_rest_route("university/v1", "manageLike", array(
        "methods" => WP_REST_Server::CREATABLE,
        "callback" => "createLike",
        'permission_callback' => '__return_true',

    ));
    // delete method
    register_rest_route("university/v1", "manageLike", array(
        "methods" => WP_REST_Server::DELETABLE,
        "callback" => "deleteLike",
        'permission_callback' => '__return_true',

    ));
}
function createLike($data)
{
    if (is_user_logged_in()) {
        $professor = sanitize_text_field($data["professorId"]);

        //this query looks for likes made by the current logged in user for the current professor

        //if user is not logged get_current_user_id, would evaluate to 0, and filtering for an ID of 0 is equivalent to not filtering for an author
        $statusQuery = new WP_Query(array(
            "author" => get_current_user_id(),
            "post_type" => "like",
            "meta_query" => array(
                array(
                    "key" => "liked_professor_id",
                    "value" => $professor,
                    "compare" => "="
                )


            )
        ));

        if ($statusQuery->found_posts == 0 && get_post_type($professor) == "professor") {
            return wp_insert_post(array(
                "post_type" => "like",
                "post_status" => "publish",
                "post_title" => "more test",
                "post_content" => "test post content",
                "meta_input" => array(
                    "liked_professor_id" => $professor,
                )
            ));
        } else {
            die("invalid professor id");
        };
    } else {
        die("only logged in users can create a like");
    }
};
function deleteLike($data)

{
    $likeId = sanitize_text_field($data["like"]);
    if (get_current_user_id() == get_post_field("post_author", $likeId) && get_post_type($likeId) == "like") {
        wp_delete_post($likeId, true);
        return "like deleted";
    } else {
        die("you do not have permission that");
    };
}




add_action("rest_api_init", "universityLikeRoutes");
