<?php
function universityRegisterSearch()
{
    register_rest_route("university/v1", "search", array(
        "methods" => WP_REST_Server::READABLE,
        "callback" => "universitySearchResults",
        'permission_callback' => '__return_true',

    ));
}
function universitySearchResults($data)
{
    $args = array(
        "post_type" => array("post", "page", "professor", "program", "event", "campus"),
        "s" => sanitize_text_field($data["term"]),


    );
    $mainQuery = new WP_Query($args);


    $results = array(
        "generalInfo" => array(),
        "programs" => array(),
        "professors" => array(),
        "events" => array(),
        "campuses" => array()
    );



    while ($mainQuery->have_posts()) {
        $mainQuery->the_post();
        $postType = get_post_type();

        switch ($postType) {

            case "program":
                $relatedCampuses = get_field("related_campus");
                if ($relatedCampuses) {
                    foreach ($relatedCampuses as $relatedCampus) {
                        array_push($results["campuses"], array(
                            "title" => get_the_title($relatedCampus),
                            "permalink" => get_the_permalink($relatedCampus),
                        ));
                    }
                }
                array_push($results["programs"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),
                    "id" => get_the_ID(),
                    "relatedCampus" => $relatedCampus
                ));

                break;
            case "professor":
                array_push($results["professors"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),
                    "image" => get_the_post_thumbnail_url(0, "professorLandscape")
                ));
                break;
            case "event":
                $eventDate = DateTime::createFromFormat("d/m/Y", get_field('event_date'));
                $theMonth =  $eventDate->format('M');
                $theDay =  $eventDate->format('d');
                $description = null;
                if (has_excerpt()) {
                    $description =  get_the_excerpt();
                } else {
                    $description =  wp_trim_words(get_the_content(), 18);
                }
                array_push($results["events"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),
                    "month" => $theMonth,
                    "day" => $theDay,
                    "description" => $description,
                ));
                break;
            case "campus":
                array_push($results["campuses"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),

                ));
                break;
            default:
                array_push($results["generalInfo"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),
                    "postType" => get_post_type(),
                    "authorName" => get_the_author(),


                ));
                break;
        }
    }

    $args = array(
        "posts_per_page" => -1,
        "post_type" => array("professor", "event"),
        "orderby" => "title",
        "order" => "asc",
        "meta_query" => array(
            "relation" => "OR",
        )
    );
    //if query returned any programs, then for each program, add meta query that returns professors with said program as related program.
    if (!empty($results["programs"])) {
        foreach ($results["programs"] as $program) {
            $metaQueryArgs =
                array(
                    "key" => "related_programs",
                    //value needs to be surrounded by double quotes because how addvanced custom fields plugin records data in the database.
                    "value" => '"' . $program["id"] . '"',
                    "compare" => "like"


                );



            array_push($args["meta_query"], $metaQueryArgs);
        }
        wp_reset_postdata();

        $relationResults = new WP_Query($args);

        //combine results of both queries

        while ($relationResults->have_posts()) {
            $relationResults->the_post();
            if (get_post_type() === "professor") {
                array_push($results["professors"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),
                    "image" => get_the_post_thumbnail_url(0, "professorLandscape")

                ));
            }
            if (get_post_type() === "event") {
                array_push($results["events"], array(
                    "title" => get_the_title(),
                    "permalink" => get_the_permalink(),
                    "month" => $theMonth,
                    "day" => $theDay,
                    "description" => $description,
                ));
            }
        };







        //array unique can scramble key values of array. Not verified yet
        $results["professors"] = array_values(array_unique($results["professors"], SORT_REGULAR));
        $results["campuses"] = array_values(array_unique($results["campuses"], SORT_REGULAR));
        $results["events"] = array_values(array_unique($results["events"], SORT_REGULAR));
    }



    return $results;
}
add_action("rest_api_init", "universityRegisterSearch");
