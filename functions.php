<?php
require get_theme_file_path("/inc/search-route.php");
require get_theme_file_path("/inc/like-route.php");
function page_banner($args = NULL)
{
    if (!$args["title"]) {
        $args["title"] = get_the_title();
    };
    // if (!$args["subtitle"]) {
    //     $args["subtitle"] = get_field("page_banner_subtitle");
    // };
    if (empty($args["subtitle"])) {
        $args["subtitle"] = get_field("page_banner_subtitle");
    };
    if (empty($args["photo"])) {
        if (get_field("page_banner_background_image")) {
            $args["photo"] = get_field("page_banner_background_image")["sizes"]["pageBanner"];
        } else {
            $args["photo"] = get_template_directory_uri() . "/images/ocean.jpg";
        }
    }
?>
    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(
                <?php echo $args["photo"] ?>)">
        </div>
        <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?php echo $args["title"] ?></h1>
            <div class="page-banner__intro">
                <p><?php echo $args["subtitle"] ?></p>
            </div>
        </div>
    </div>
<?php
}
function custom_theme_files()
{
    wp_enqueue_script('googleMap', "//maps.googleapis.com/maps/api/js?key=AIzaSyBJ0RieUI2DQbYHM4VomPp-PqeIHadjpb8", null, microtime(), true);
    wp_enqueue_script('main-uni-js', get_theme_file_uri("js/scripts-bundled.js"), null, microtime(), true);
    // wp_enqueue_script('main-uni-js', get_theme_file_uri("js/scripts.js"), null, microtime(), true);


    wp_enqueue_style('google_fonts', "//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i");
    wp_enqueue_style('font_awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    wp_enqueue_style('main_style', get_stylesheet_uri(), null, microtime());
    wp_localize_script(
        "main-uni-js",
        "universityData",
        array(
            "root_url" => get_site_url(),
            "nonce" => wp_create_nonce("wp_rest")
        )
    );
}
function custom_theme_features()
{
    add_theme_support("title-tag");
    add_theme_support("post-thumbnails");
    // add_image_size("professorPortrait", 300, 290, true);
    add_image_size("professorPortrait", 300, 350, true);
    // add_image_size("professorLandscapre", 200, 100, true);
    add_image_size("professorLandscape", 350, 300, true);
    add_image_size("pageBanner", 1500, 350, true);


    // register_nav_menu( "headerMenuLocation", "Header Menu Location");
    // register_nav_menu( "exploreFooterMenu", "Explore Footer Menu" );
    // register_nav_menu( "learnFooterMenu", "Learn Footer Menu" );
};
function which_template_is_loaded()
{
    if (is_super_admin()) {
        global $template;
        print_r($template);
    }
}
function university_adjust_queries($query)
{
    $today = date("Ymd");


    if (!is_admin() && $query->is_main_query()) {


        if (is_post_type_archive("program")) {

            $query->set(
                "orderby",
                "title"
            );
            $query->set(
                "posts_per_page",
                -1
            );
            $query->set(
                "order",
                "asc"
            );
        }




        if (is_post_type_archive("event")) {

            $query->set(
                "orderby",
                "meta_value_num"
            );
            $query->set(
                "order",
                "asc"
            );
            $query->set(
                "meta_query",
                array(
                    array(
                        "key" => "event_date",
                        "value" => $today,
                        "compare" => ">=",
                        "type" => "numeric"

                    )
                )
            );
        }
    }
}

function universityMapKey($api)
{
    $api['key'] = 'AIzaSyBJ0RieUI2DQbYHM4VomPp-PqeIHadjpb8';
    return $api;
}
add_filter('acf/fields/google_map/api', 'universityMapKey');
function university_custom_rest()
{
    register_rest_field("post", "authorName", array(
        "get_callback" => function () {
            return get_the_author();
        }
    ));
    register_rest_field("note", "userNoteCount", array(
        "get_callback" => function () {
            return count_user_posts(get_current_user_id(), "note");
        }
    ));
}
function redirectSubsTofrontend()
{
    $currentUser = wp_get_current_user();
    if (count($currentUser->roles) === 1 && $currentUser->roles[0] === "subscriber") {
        wp_redirect(site_url("/"));
        exit;
    }
}
function noSubsAdminBar()
{
    $currentUser = wp_get_current_user();
    if (count($currentUser->roles) === 1 && $currentUser->roles[0] === "subscriber") {
        show_admin_bar(false);
    }
}

function ourHeaderUrl()
{
    return esc_url(site_url("/"));
}
function ourHeaderText()
{
    return get_bloginfo("name");
}

function ourLoginCSS()
{
    wp_enqueue_style('main_style', get_stylesheet_uri(), null, microtime());
    wp_enqueue_style('google_fonts', "//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i");
}

function makeNotePrivate($data, $postArr)
{
    if ($data["post_type"] == "note") {
        if (count_user_posts(get_current_user_id(), "note") > 4 && !$postArr["ID"]) {
            die("you have reached your post limit");
        }
        $data['post_content'] = sanitize_textarea_field($data["post_content"]);
        $data['post_title'] = sanitize_textarea_field($data["post_title"]);
    }
    if ($data["post_type"] == "note" && $data["post_status"] != "trash") {
        $data["post_status"] = "private";
    }


    return $data;
}
function themename_customize_register($wp_customize)
{

    $wp_customize->add_section('themename_color_scheme', array(
        'title'    => __('Color Scheme', 'themename'),
        'description' => 'test',
        'priority' => 120,
    ));

    //  =============================
    //  = Text Input                =
    //  =============================
    $wp_customize->add_setting('themename_theme_options[text_test]', array(
        'default'        => 'value_xyz',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));
}

add_action('customize_register', 'themename_customize_register');



add_action("wp_enqueue_scripts", "custom_theme_files");
add_action("after_setup_theme", "custom_theme_features");
add_action('wp_footer', 'which_template_is_loaded');
add_action("pre_get_posts", "university_adjust_queries");
add_action("rest_api_init", "university_custom_rest");
//redirect subscribers accounts out of dashboard and onto homepage

add_action("admin_init", "redirectSubsToFrontend");
add_action("wp_loaded", "noSubsAdminBar");

//customize login screen
add_filter("login_headerurl", "ourHeaderUrl");
add_filter('login_headertext', 'ourHeaderText');
add_action("login_enqueue_scripts", "ourLoginCSS");

//force notes to be private
add_filter("wp_insert_post_data", "makeNotePrivate", 10, 2);
